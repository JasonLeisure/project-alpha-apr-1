from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignupForm(UserCreationForm):
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput()
    )

    class Meta:
        model = User
        fields = (
            "username",
            "password1",
            "password2",
            "password_confirmation",
        )


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())
