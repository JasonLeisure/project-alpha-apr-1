from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Project


@login_required
class ProjectListView(ListView):
    model = Project
    template_name = "project_list.html"
    context_object_name = "projects"

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(members=self.request.user)


@login_required
def redirect_to_project_list(request):
    return redirect("project_list")


@login_required
def project_list(request):
    projects = Project.objects.filter(members=request.user)
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


@login_required
def home(request):
    projects = Project.objects.filter(members=request.user)
    context = {"projects": projects}
    return render(request, "projects/home.html", context)
